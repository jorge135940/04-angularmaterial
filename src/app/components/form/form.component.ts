import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  formulario!: FormGroup;
  contenido2: string[] = []
  form1!: FormGroup;
  lista2!: string;


  constructor(private fb: FormBuilder) { 
    this.crearFormulario();
      
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      cajaG: [''],
      cajaP: this.fb.array([[]]),
      nombre: ['', [Validators.requiredTrue]]
    })
  }

  ngOnInit(): void {
  }

  get contenidoP(){
    return this.formulario.get('cajaP') as FormArray
  }

  agregar():void{
    this.contenidoP.push(this.fb.control(''))
  }
  borrarCaja(i: number): void{
    this.contenidoP.removeAt(i);
  }
  limpiarCajaP(i:number){
    console.log(this.formulario.value.cajaP[i]);
    this.formulario.value.cajaP[i]
    
  }

  limpiar(): void {
    this.contenido2 = ['']
    this.formulario.reset
  }
  limpiarCajaG(): void{
    this.contenido2 = ['']
  }

  guardar(): void{
    console.log('guardar');
    this.contenido2 = this.formulario.value.cajaP
  }

  limpiarCajas(): void { 
    // volvemos el valor de los input vacias  
   this.formulario!.reset({
     i: ''
   })
   this.lista2 = '';
  }

}

